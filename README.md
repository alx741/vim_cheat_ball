VIM Cheat Ball
==============
A nice desktop paper ornament with Vim commands fast references.

![Vim cheat ball](http://i.imgur.com/tPfSoAS.jpg)


Download
--------

In order to download the *pdf* files please use the `Download ZIP` option in
github (at the right of the screen).

Or use `wget <pdf link>`


Using
-----

- Print the file *small.pdf* or *large.pdf* (A4 format)
- Cut following the marks
- Fold following the marks
- Glue it!


License
-------

This resource is licensed under the Creative Commons Attribution-ShareAlike 4.0
International license.

By: Daniel Campoverde [Alx741]

mailto: alx741@riseup.net

[Silly Bytes](http://www.silly-bytes.blogspot.com)
